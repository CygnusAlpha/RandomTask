
randomtask: build
	docker-compose run --rm randomtask
	docker-compose logs -f

rabbit-client: build
	docker-compose run --rm randomtask python rabbit_pika_client.py

rabbit-pubsub: build
	docker-compose run --rm randomtask python rabbit_pika_pubsub.py

rabbit-routes: build
	docker-compose run --rm randomtask python rabbit_pika_routes.py

build:
	docker-compose build
