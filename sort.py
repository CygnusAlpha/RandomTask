import random
from randomtask import RandomTask, app
from celery.execute import send_task

i = app.control.inspect()
print(i.registered_tasks())

t=RandomTask()

numbers = list(range(1,11))
random.shuffle(numbers)
for i in numbers:
    print("Queued: {}".format(i))
    t.sort.delay(i)
