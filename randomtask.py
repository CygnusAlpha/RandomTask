import os
import time
from celery import Task
from celery import Celery

# Use env variables to configure celery
host = os.environ['RABBITMQ_HOST']
user = os.environ['RABBITMQ_DEFAULT_USER']
passwd = os.environ['RABBITMQ_DEFAULT_PASS']
vhost = os.environ['RABBITMQ_DEFAULT_VHOST']

app = Celery(
    'tasks',
    broker='pyamqp://{}:{}@{}/{}'.format(user, passwd, host, vhost),
)

class RandomTask(Task):
    @app.task(name='randomtask.sort', bind=True)
    def sort(self, i):
        time.sleep(i)
        print("{}".format(i))
