#!/bin/sh
set -e

# Wait for rabbit
until nc -z rabbit 5672; do
    echo "$(date) - waiting for Rabbit..."
    sleep 7
done

echo "$(date) - Ready.."
echo "Pwd: `pwd`"
echo "Run: $@"
exec "$@"
