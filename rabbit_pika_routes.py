"""
    `docker-compose run randomtask python rabbit_pika_client.py`
"""
import os
import pika

host = os.environ['RABBITMQ_HOST']
user = os.environ['RABBITMQ_DEFAULT_USER']
password = os.environ['RABBITMQ_DEFAULT_PASS']
vhost = os.environ['RABBITMQ_DEFAULT_VHOST']

credentials = pika.PlainCredentials(user, password)
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host,
    5672,
    vhost,
    credentials)
)
channel = connection.channel()

channel.exchange_declare(exchange="directions", exchange_type="direct")


##############
# SUBSCRIBER #
##############

# If queue is empty, a random queuename is created
subscriber = channel.queue_declare(queue='', exclusive=True)
# Subscribe this queue to the events exchange
channel.queue_bind(exchange='events', queue=subscriber.method.queue, routing_key="left")

def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)

channel.basic_consume(
    queue=subscriber.method.queue,
    auto_ack=True,
    on_message_callback=callback
)

##############
# PUBLISHER #
##############

channel.basic_publish(
    exchange='directions',
    routing_key='left',
    body='Hello World!'
)
channel.basic_publish(
    exchange='directions',
    routing_key='left',
    body='Lost!'
)

channel.start_consuming()
