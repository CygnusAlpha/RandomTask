Random Task
===========

![Fermats Library](17CB8422-8C63-4008-8E18-ECCDE0805EBB.jpg)

Random number sorting silliness.

See: 17CB8422-8C63-4008-8E18-ECCDE0805EBB.jpg

Does however demonstrate celery, rabbit and sample tasks.

Prerequisites
-------------

* docker
* docker-compose

### Docker

Best to use a nice recent version from docker-ce. Debian & derivatives use a really old version.

```
# Debian
sudo -s
apt-get remove docker docker-engine docker.io
apt-get update
apt-get install -y \
  curl \
  linux-image-extra-$(uname -r) \
  linux-image-extra-virtual
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get install -y docker-ce
```

### Docker Compose

```
sudo -s
curl -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```


Run
---

* docker-compose build
* docker-compose up

Stop
----

* docker-compose down
