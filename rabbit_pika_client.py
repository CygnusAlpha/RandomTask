"""
    `docker-compose run randomtask python rabbit_pika_client.py`
"""
import os
import pika

host = os.environ['RABBITMQ_HOST']
user = os.environ['RABBITMQ_DEFAULT_USER']
password = os.environ['RABBITMQ_DEFAULT_PASS']
vhost = os.environ['RABBITMQ_DEFAULT_VHOST']

queue = 'ramdomtask'

credentials = pika.PlainCredentials(user, password)
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host,
    5672,
    vhost,
    credentials)
)
channel = connection.channel()

channel.queue_declare(queue=queue)

##############
# PUBLISHER #
##############

channel.basic_publish(
    exchange='',
    routing_key=queue,
    body='Hello World!'
)

############
# CONSUMER #
############

def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)

channel.basic_consume(
    queue=queue,
    auto_ack=True,
    on_message_callback=callback
)

channel.start_consuming()
