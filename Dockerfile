FROM python:3-alpine
MAINTAINER custard@cpan.org
ENV project=randomtask

WORKDIR /${project}
COPY run.sh /${project}/
COPY requirements.txt /${project}/
RUN pip install -r requirements.txt
COPY *.py /${project}/

ENTRYPOINT ["./run.sh"]
CMD ["celery", "-A", "randomtask", "worker"]
